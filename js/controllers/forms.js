function attachFormEvents (form) {
  form.addEventListener('submit', (event) => {
    event.preventDefault()
    const submitButton = form.querySelector('.control .button[type="submit"]')

    submitButton.classList.add('is-loading')

    const qs = new URLSearchParams(new FormData(form))

    fetch(form.getAttribute('action'), {
      method: 'POST',
      body: qs.toString(),
      mode: 'cors',
      cache: 'no-cache',
      headers: {
        'Accept': 'application/json',
      }
    })
      .then(response => {
        if (response.ok) {
          window.location.hash = '#form-success'
          form.reset()
        }
        else {
          throw Error(response)
        }
      })
      .catch(() => {
        window.location.hash = '#form-error'
      })
      .finally(() => {
        submitButton.classList.remove('is-loading')
        form.scrollIntoView({ behavior: 'smooth', block: 'start' })
      })
  })
}

function preselectEvent (search) {
  const params = new URLSearchParams(search)
  const eventId = params.get('event')
  const option = document.querySelector(`option[data-event="${eventId}"]`)

  if (option) {
    const { index } = option
    option.parentElement.selectedIndex = index
  }
}

document.addEventListener('DOMContentLoaded', () => {
  Array.from(document.querySelectorAll('.nextcloud-form'))
    .forEach(form => attachFormEvents(form))

  preselectEvent(document.location.search)
})
