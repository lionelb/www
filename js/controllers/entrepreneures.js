import Fuse from 'https://cdn.skypack.dev/pin/fuse.js@v6.6.2-ITZQvNnakiB8msFu2fHD/mode=imports,min/optimized/fusejs.js'

const directoryData = JSON.parse(document.querySelector('#directory-data').innerText)
const searchField = document.querySelector('#search-field')
const directoryEntry = Array.from(document.querySelectorAll('article[data-id]'))

// via https://github.com/krisk/Fuse/issues/415#issuecomment-634348136
const removeDiacritics = str => str.normalize('NFD').replace(/[\u0300-\u036F]/g, '')

const index = new Fuse(directoryData, {
  includeScore: true,
  minMatchCharLength: true,
  threshold: 0.3,
  ignoreLocation: true,
  keys: [
    'first_name',
    'last_name',
    'title',
    {
      name: 'presentation',
      getFn: (person) => removeDiacritics(person.presentation)
    }
  ]
})

searchField.addEventListener('input', (event) => {
  const value = String(event.target.value).trim()
  const results = index.search(removeDiacritics(value))

  console.log('Results for %s: %o', value, results)

  directoryEntry.forEach(entry => {
    const id = parseInt(entry.dataset.id, 10)
    const status = value === '' ? true : results.find(({ refIndex }) => refIndex === id)

    if (status) {
      entry.classList.remove('is-hidden')
    }
    else {
      entry.classList.add('is-hidden')
    }
  })
})


