import { select } from "https://cdn.skypack.dev/d3-selection@3"
import { arc } from "https://cdn.skypack.dev/d3-shape@3"
import { scaleBand, scaleRadial } from "https://cdn.skypack.dev/d3-scale@4"
import { schemeCategory10 } from "https://cdn.skypack.dev/d3-scale-chromatic@3"

const svg = select('.graphique-entrepreneures svg')
const data = JSON.parse(document.querySelector('.graphique-entrepreneures + script[type="application/json"]').textContent)

const innerRadius = 5
const outerRadius = svg.attr('width') * 0.95

const x = scaleBand()
  .range([0, 2 * Math.PI])
  .align(0)
  .domain(data.map((d, i) => d.Matricule));

// Y scale
const y = scaleRadial()
    .range([innerRadius, outerRadius])
    .domain([0, 10000])

svg
  .append("g")
    .attr('transform', `translate(${outerRadius / 2},${outerRadius / 2})`)
  .selectAll("path")
  .data(data)
  .join("path")
    .on('mouseover', handeMouseOver)
    .on('mouseout', handleMouseOut)
    .attr("fill", (d, i) => schemeCategory10[i % 10])
    .attr("d", arc()     // imagine your doing a part of a donut plot
      .innerRadius(innerRadius)
      .outerRadius((d) => Math.max(Math.round(Math.random() * 100), Math.round(Math.random() * (outerRadius / 2))))
      .startAngle((d) => x(d.Matricule))
      .endAngle((d) => x(d.Matricule) + x.bandwidth())
      .padAngle(0.04))
    .append('text')
      .text((d) => d.Projet)

function handeMouseOver (d, i) {
  select(this).style('opacity', 0.25).style('cursor', 'pointer')
}

function handleMouseOut (d, i) {
  select(this).style('opacity', 1).style('cursor', 'default')
}
