const EleventyFetch = require("@11ty/eleventy-fetch")

const { NEXTCLOUD_BASE_URL, CI } = process.env

module.exports = function NextcloudFormsPlugin (eleventyConfig, { user, password }) {
  eleventyConfig.addGlobalData("ncForms", async () => {
    const ncForms = await getAllForms({ user, password })
    !CI && listForms(ncForms)
    return ncForms
  })
}

function makeRequest (path, { user, password, body }) {
  return EleventyFetch(`${NEXTCLOUD_BASE_URL}/ocs/v2.php/apps/forms/api/v1.1${path}`, {
    duration: body ? '0' : '1d',
    type: 'json',
    dryRun: body ? true : false,
    fetchOptions: {
      method: body ? 'POST' : 'GET',
      headers: {
        'OCS-APIRequest': true,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Basic ${btoa(`${user}:${password}`)}`
      },
      // optionnally adds a body to the request
      ...(body ? { body } : {})
    }
  })
}

function getForm ({ id, user, password }) {
  return makeRequest(`/form/${id}`, { user, password })
}

async function getAllForms ({ user, password }) {
  const forms = await Promise.all([
    makeRequest('/forms', { user, password }),
    makeRequest('/shared_forms', { user, password }),
  ])
  .then(([forms, sharedForms]) => [ ...forms.ocs.data, ...sharedForms.ocs.data ])
  .then((forms) => forms.filter(({ expires }) => expires === 0))

  return Promise.all(forms.map(({ id }) => getForm({ id, user, password })))
    .then(([...forms]) => forms.map(form => form.ocs.data))
    .then(forms => forms.reduce((obj, form) => ({ ...obj, [form.id]: form }), {}))
}

function listForms (forms) {
  console.log('\nNextCloud Forms:')
  Object.values(forms).forEach(({ id, hash, title }) => {
    console.log(`#${hash} • id ${id.toString().padEnd(4, ' ')}: ${title}`)
  })
}

exports.listForms = listForms
exports.getAllForms = getAllForms
