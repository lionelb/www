const EleventyFetch = require("@11ty/eleventy-fetch")

const { NEXTCLOUD_BASE_URL, CI } = process.env

module.exports = function NextcloudFormsPlugin (eleventyConfig, { user, password }) {
  eleventyConfig.addGlobalData("ncUsers", async () => {
    const ncUsers = await getAllUsers({ user, password })
    !CI && listUsers(ncUsers)
    return ncUsers
  })
}

function makeRequest (path, { user, password, body }) {
  return EleventyFetch(`${NEXTCLOUD_BASE_URL}/ocs/v2.php${path}`, {
    duration: body ? '0' : '1d',
    type: 'json',
    dryRun: body ? true : false,
    fetchOptions: {
      method: body ? 'POST' : 'GET',
      headers: {
        'OCS-APIRequest': true,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Basic ${btoa(`${user}:${password}`)}`
      },
      // optionnally adds a body to the request
      ...(body ? { body } : {})
    }
  })
}

function getUser ({ id, user, password }) {
  return makeRequest(`/form/${id}`, { user, password })
}

async function getAllUsers ({ user, password }) {
  const users = await makeRequest('/cloud/users/details?format=json', { user, password })
    .then(response => Object.values(response.ocs.data.users))
    .then(users => users.filter(({ id }) => id))

  return users
}

function listUsers (users) {
  console.log('\nNextCloud Users:')
  Object.values(users).forEach(({ id, role, displayname }) => {
    console.log(`- ${id}: ${displayname} (${role})`)
  })
}

exports.listUsers = listUsers
exports.getAllUsers = getAllUsers
