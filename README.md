# Solstice.coop

Documentation de travail et code source du site web de la Coopérative d'Activité et d'Emploi Solstice.

**Raccourcis** :

- [👁 Prévisualiser le site](https://solstice-www-next.netlify.app/)
- [📨 Travail à faire](https://gitlab.com/solstice.coop/www/-/boards)
- [⏳ Temps passé](le-temps-qui-passe.csv)
