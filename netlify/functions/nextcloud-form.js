const { parse } = require('qs')
const nodeFetch = require('node-fetch')

const { NEXTCLOUD_BASE_URL } = process.env
const { NEXTCLOUD_APPLICATION_USER:user } = process.env
const { NEXTCLOUD_APPLICATION_PASSWORD:password } = process.env

const LAMBDA_FUNCTION_CORS_ORIGINS = [
  'http://localhost:8080',
  'https://v2.solstice.coop',
  'https://www.solstice.coop',
  'https://solstice.coop',
]

/**
 * Returns a Map-style object from a parsed query string
 * For ?answers[3][]=Hello, we then have
 * <- [undefined, undefined, ['Hello']]
 * -> { '3': ['Hello'] }
 * @see https://github.com/nextcloud/forms/blob/master/docs/API.md#insert-a-submission
 * @param {*} qsParsedAnswers
 * @returns
 */
function prepareAnswers (qsParsedAnswers) {
  return Object.fromEntries(
    qsParsedAnswers
      // prepare for [key, value] fromEntries expectations
      .map((value, index) => ([index, value]))
      // remove sparse entries
      .filter(entry => entry)
  )
}

exports.handler = async function ({ httpMethod, body, headers:requestHeaders, isBase64Encoded }) {
  const accessControlAllowOrigin = LAMBDA_FUNCTION_CORS_ORIGINS.includes(requestHeaders.origin)
    ? requestHeaders.origin
    : LAMBDA_FUNCTION_CORS_ORIGINS.at(-1)

  const headers = {
    "Access-Control-Allow-Headers" : "Content-Type",
    "Access-Control-Allow-Origin": accessControlAllowOrigin,
    "Access-Control-Allow-Methods": "OPTIONS,POST"
  }

  if (httpMethod !== 'POST') {
    return {
      statusCode: 400,
      headers
    }
  }

  const { answers, formId } = parse(isBase64Encoded ? atob(body) : body, { allowSparse: true })

  try {
    const response = await nodeFetch(`${NEXTCLOUD_BASE_URL}/ocs/v2.php/apps/forms/api/v1.1/submission/insert`, {
      method: 'POST',
      headers: {
        'OCS-APIRequest': true,
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': `Basic ${btoa(`${user}:${password}`)}`
      },
      body: JSON.stringify({ formId, answers/*: prepareAnswers(answers)*/ })
    })

    const isOk = response.ok

    if (!isOk) {
      response.text().then(error => console.error('node-fetch error for %s with %s: %o ', user, body, error))
    }

    return {
      statusCode: isOk ? 204 : 400,
      headers
    }
  }
  catch (error) {
    console.error('catch error %o', error)

    return {
      statusCode: 500,
      headers
    }
  }
}
