const EleventyFetch = require("@11ty/eleventy-fetch")

module.exports = async function() {
  return await EleventyFetch("https://formasol.solstice.coop/api/catalogue.json", {
    duration: '1d',
    type: 'json'
  })
};
