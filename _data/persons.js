const EleventyFetch = require("@11ty/eleventy-fetch")



module.exports = async function() {
  // limit is 100 items for now
  // @see https://airtable.com/appywAxghnSzkqNEp/api/docs#curl/table:liste:list
  const persons = await EleventyFetch(`https://formasol.solstice.coop/api/users.json`, {
    duration: '1d',
    type: 'json'
  })

  return persons.filter(({ first_name }) => !/admin/i.test(first_name))
};
