const EleventyFetch = require("@11ty/eleventy-fetch")
const ICAL = require("ical.js")

const { NEXTCLOUD_BASE_URL } = process.env
const now = new ICAL.Time.now()

module.exports = async function() {
  const options = {
    type: 'text',
    duration: '1d'
  }

  if(process.env.ELEVENTY_SERVERLESS) {
    options.duration = "0";
    options.directory = "cache";
  }

  const data = await EleventyFetch(`${NEXTCLOUD_BASE_URL}/remote.php/dav/public-calendars/mtQg7RBgeLPiLaEz?export`, options)

  const cal = new ICAL.Component(ICAL.parse(data))

  return cal.getAllSubcomponents('vevent')
    .map(event => new ICAL.Event(event))
    .sort((a, b) => a.startDate.compare(b.startDate))
    // .slice(-5)
    .filter(event => event.startDate.compare(now) >= 0)
};


