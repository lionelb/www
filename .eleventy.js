const { join, normalize } = require('path')
const markdownItFootnote = require('markdown-it-footnote')
const markdownItAttrs = require('markdown-it-attrs')
const markdownItContainer = require('markdown-it-container')
const eleventyNavigationPlugin = require("@11ty/eleventy-navigation")
const EleventyVitePlugin = require("@11ty/eleventy-plugin-vite")
const SolsticePlugin = require('./js/filters.js')
const { totalTime } = require('./js/filters.js')

const NextcloudFormsPlugin = require('./js/plugins/nextcloud-forms.js')
const NextcloudUsersPlugin = require('./js/plugins/nextcloud-users.js')

const { NEXTCLOUD_BASE_URL } = process.env
const { NEXTCLOUD_APPLICATION_USER:user } = process.env
const { NEXTCLOUD_APPLICATION_PASSWORD:password } = process.env

module.exports = function (eleventyConfig) {
  eleventyConfig.addPlugin(eleventyNavigationPlugin)

  eleventyConfig.addPassthroughCopy("img")
  eleventyConfig.addPassthroughCopy("js/controllers/**/*.js")
  eleventyConfig.addPassthroughCopy("css/**/*.{css,woff2}")

  eleventyConfig.addGlobalData("layout", "contenu.njk")
  eleventyConfig.addGlobalData("navOptions", {
    includeSelf: true,
  })

  eleventyConfig.addGlobalData("nextcloudUrl", NEXTCLOUD_BASE_URL)
  eleventyConfig.addPlugin(NextcloudFormsPlugin, { user, password })
  eleventyConfig.addPlugin(NextcloudUsersPlugin, { user, password })

  eleventyConfig.addGlobalData("temps", async () => totalTime('./le-temps-qui-passe.csv'))

  eleventyConfig.amendLibrary("md", mdLib => {
    mdLib.disable('code')
    mdLib.use(markdownItFootnote)
    mdLib.use(markdownItAttrs, { allowedAttributes: ['id', 'class', 'target']})
    mdLib.use(markdownItContainer, 'info', {
      render (tokens, idx) {
        if (tokens[idx].nesting === 1) {
          return `<article class="message is-info">
            <div class="message-header">
              <p>${tokens[idx].info.trim().replace(/^\s*info\s*/, '')}</p>
            </div>
            <div class="message-body">
          `
        }
        else {
          return `</div></article>`
        }
      }
    })
  })

  eleventyConfig.addCollection("posts", collectionApi => {
    return collectionApi
      .getFilteredByGlob('pages/cooperative/temps-forts/*.md')
      .filter(({ inputPath }) => inputPath.endsWith('index.md') === false)
      .sort((a, b) => b.date - a.date)
  })

  eleventyConfig.addPlugin(require("eleventy-plugin-dart-sass"), {
    sassLocation: normalize(join(__dirname, 'css/_sass/')),
    sassIndexFile: '_index.scss',
    outDir: normalize(join(__dirname, '_site')),
    includePaths: ['**/*.scss', '!node_modules/**', 'node_modules/bulma'],
    cacheBreak: Date.now(),
  })


  eleventyConfig.addPlugin(SolsticePlugin)
  eleventyConfig.addPlugin(EleventyVitePlugin)

  return {
    markdownTemplateEngine: "njk",
    htmlTemplateEngine: "njk",
    dir: {
      input: "pages",
      includes: "../_layouts",
      layouts: "../_layouts",
      data: "../_data",
    }
  }
}
