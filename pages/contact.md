---
title: Contacter une personne chez Solstice
eleventyNavigation:
  key: Contact
  parent: Accueil
  hidden: true
---

{%- from "form.html" import form with context -%}

# Contactez-nous

- [prendre rendez-vous avec un·e accompagnant·e ?](#accompagnant)
- [en savoir plus sur le portage de vos formations si vous êtes OF non certifié ?](#organisme-de-formation)
- [en savoir plus sur la formation de formateur·trice ?](#formation-formateurice)
- [en savoir plus tout court ?](#autre)  

## Prendre rendez-vous avec un·e accompagnant·e ? { #accompagnant }

{% include 'contact/rendez-vous-accompagnant.njk' %}

## En savoir plus sur le portage de vos formations si vous êtes OF non certifié ? { #organisme-de-formation }

{{ form(id=8, title=false, submit="J'envoie ma demande", action='https://v2.solstice.coop/.netlify/functions/nextcloud-form') }}

## En savoir plus sur la formation de formateur·ice ? { #formation-formateurice }

{{ form(id=10, title=false, submit="J'envoie ma demande", action='https://v2.solstice.coop/.netlify/functions/nextcloud-form') }}

## En savoir plus tout court ! { #autre }

{{ form(id=2, title=false, submit="J'envoie mon message", action='https://v2.solstice.coop/.netlify/functions/nextcloud-form') }}
