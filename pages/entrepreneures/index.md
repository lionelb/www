---
title: Annuaire d'entrepreneur·es de la vallée de la Drôme
eleventyNavigation:
  key: Les entrepreneur·es
  parent: Accueil
  order: 4
scripts:
  - /js/controllers/entrepreneures.js
---
<script type="application/json" id="directory-data">{{ persons | pickattr('first_name', 'last_name', 'presentation', 'title') | dump | safe }}</script>

<div class="control mb-5">
  <input class="input is-medium" type="search" id="search-field" name="q" autocomplete="off" placeholder="Filter par prénom, nom d'activité, etc.">
</div>

{% set comma = joiner() %}

:::info Annuaire en cours de réalisation
L'annuaire détaillé est en train d'être alimenté·e par les {{ entrepreneures.length }} entrepreneur·es.

Il n'est pas encore un reflet exhaustif des personnes qui composent la coopérative, par exemple : {% for p in entrepreneures.slice(0, 5) %}{{ comma() }} {{ p.Projet }}{% endfor %}…
:::

{% for person in persons %}
<article class="box directory-entry" data-id="{{ loop.index0 }}">
  <div class="media">
    {%- if person.photo or person.logo -%}
    <figure class="media-left ml-0">
      <p class="image is-256x256 is-contained">
        <img src="{{ person.photo or person.logo }}" alt="Photo de {{ person.first_name }}">
      </p>
    </figure>
    {%- endif -%}
    <div class="media-content">
      <h3 class="title">
        {{ person.first_name }} {{ person.last_name }}
      </h3>
      <div class="content">{{ person.presentation | safe }}</div>
      <ul class="discrete">
      {%- if person.title -%}
      <li>
        <strong>Activités/Métiers</strong> : {{ person.title }}
      </li>
      {%- endif -%}
      {%- if person.website -%}
      <li>
        <span class="icon"><i class="fas fa-home"></i></span>
        <a href="{{ person.website }}" target="_blank" rel="noreferrer noopener">{{ person.website }}</a>
      </li>
      {%- endif -%}
      {#- if person.email -%}
      <li>
        <strong>Email</strong> : <a href="mailto:{{ person.email }}">{{ person.email }}</a>
      </li>
      {% endif #}
      {#- if person.phone -%}
      <li>
        <strong>Téléphone</strong> : <a href="tel:{{ person.phone }}">{{ person.phone }}</a>
      </li>
      {%- endif -#}
      </ul>
    </div>
  </div>
</article>
{% endfor %}

