---
title: Page introuvable
permalink: 404.html
---

## Page introuvable

Oups, le lien que vous avez suivi nous est inconnu.

On a modifié le site en profondeur en octobre 2022, ceci explique peut-être celà.\
Jetez un œil dans le menu de navigation principal, vous y trouverez peut-être votre bonheur.

Sinon, voici quelques liens utiles : 

- [S'inscrire à une prochaine réunion d'informations](/nous-rejoindre/reunion-information/)
- [Tous·tes nos entrepreneur·es](/entrepreneures/)
- [Nous contacter](/contact/)
