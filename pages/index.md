---
layout: home.njk
seo:
  title: Solstice, coopérative d'activité et d'entrepreneuriat (CAE) dans la Drôme
title: |
  Une communauté d’entrepreneur·e·s<br>en Biovallée
subtitle: |
  Solstice SCOP propose un statut alternatif et un accompagnement dédié aux personnes qui souhaitent entreprendre et travailler autrement.
extra: |
  Coopérative généraliste, Solstice permet aux porteuses et porteurs d’activités économiques de se développer dans une dynamique de coopération et de solidarité.
eleventyNavigation:
  key: Accueil
  order: 1
figma: https://www.figma.com/file/cuBvusShQ85ecBMBIqwnlf/Site?node-id=116%3A378
---

## Travailler autrement, rester indépendant·e et devenir salarié·e  

Chez Solstice, nous considérons qu'une activité dans une CAE fonctionne comme une entreprise : elle doit être lucrative, dégager du résultat, être rentable, et permettre d'investir. Pour notre société coopérative, la recherche de profit économique reste subordonnée à l'épanouissement des coopératrices et coopérateurs salarié·es. L'activité économique hébergée dans la coopérative peut être saisonnière, s'adapter aux marchés, et au rythme de l'activité que chacun·e peut avoir envie d'accélérer ou ralentir sur une année.

#### Des moyens mutualisés et des solutions pour faciliter votre activité au quotidien.

- Vous êtes accompagné·e individuellement à chaque étape de votre parcours entrepreneurial : vous sécurisez le développement de votre activité.
- Les dimensions administratives, comptables et juridiques sont réalisées par de vraies personnes qui vous répondront toujours : vous restez concentré·es sur votre cœur de métier.
- Vous vous formez, individuellement, et bénéficiez des compétences et des expertises de vos pairs et de la communauté.
- Vous utilisez des outils de gestion numériques dédiés en constante amélioration.

[En savoir plus sur le fonctionnement au quotidien](/cooperative/fonctionnement/){.button .is-cta}

## La Coopérative d'activité et d'entrepreneur·es
propose un modèle d’entrepreneuriat salarié unique, qui permet de créer et de développer sa propre activité dans un cadre autonome, coopératif et sécurisé. En CAE, votre activité économique vous permet de vous salarier au sein de la coopérative : vous signez un contrat de travail, vous persever un salaire et vous bénéficiez d'une protection sociale. [Découvrir et intégrer une CAE](https://www.les-cae.coop/system/files/inline-files/Entreprendre%20en%20CAE%20-%20Plaquette%202022%20VDiff.pdf)

## Solstice est organisme de formation certifié Qualiopi
[En savoir plus sur Solstice Organisme de formation](/formations/){.button .is-cta}

## Une entreprise partagée
Solstice est une SCOP, Société COopérative et Participative, une Coopérative d’Activité et d’Entrepreneur·es (CAE) et une Société Anonyme. Nos statuts impliquent la participation des personnes au sociétariat avec un droit de vote égal en assemblée générale. Rajoutons que cette forme nous permet de porter une vision innovante de l’entrepreneuriat, à la fois humaine et économiquement viable.  


Cette action est cofinancée par l'Union européenne avec le Fonds Social Européen (FSE).
![Union Européenne](/img/logo_ue.jpg)
![Aide européenne aux régions](/img/logo_feader.jpg)
![Région Auvergne-Rhône-Alpes](/img/logo_aura.jpg)
![Fédération des CAE](/img/logo_cae.png)
