---
title: Catalogue des formations d'entrepreneur·es Solstice
eleventyNavigation:
  key: catalogue
  title: Catalogue de nos formations
  parent: Organisme de formation
---

{% include 'formations/catalogue.njk' %}
