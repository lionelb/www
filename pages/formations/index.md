---
title: Organisme de formation
eleventyNavigation:
  key: Organisme de formation
  parent: Accueil
  order: 3
---

# Organisme de formation

Solstice SCOP est un organisme de formation certifié Référentiel National Qualité (Qualiopi) au titre des actions de formation depuis le 1er octobre 2021. Nous développons et portons des activités de Formation Professionnelle Continue avec une exonération de TVA et un accès aux fonds de financements publics de la formation.

## Vous êtes formatrice·teur ?

<div class="columns">
<div class="column is-one-third">

Solstice héberge **vos formations**.

[En savoir plus ? Contactez-nous !](/contact/#contact){ .button .is-cta }
</div>

<div class="column">

Chaque année, une quarantaine d'entrepreneur·es solsticien·nes hébergent leur activité de formation dans la coopérative. Le catalogue proposé couvre une vingtaine de thématiques.

Chaque formateur.trice est en charge de son portefeuille client·es, communique auprès de ses client·es (web, mail, plaquette…) et peut proposer des formations au catalogue ou sur mesure (à la demande, en fonction des besoins des clients), en présentiel et/ou à distance. Certains contenus et tarifs peuvent être personnalisables (notamment pour les Solsticien.nes).

</div>
</div>

## Bientôt disponible, le catalogue des formations pro

• Accompagnement, relation d’aide
• Bien-être
• Communication inter-personnelle
• Compostage
• Data sciences et intelligence artificielle
• Écriture et communication écrite
• Gestion de projets participatifs
• Intelligence collective
• Intervention en institutions
• Langues
• Management, communication, RH
• Médiation
• Ostéopathie animale
• Pédagogie
• Psychologie, psychothérapie, psycho corporelle
• RGPD
• Techniques textiles
• Web, digital, bureautique

## Vous êtes organisme de formation non-certifié Qualiopi  ?

<div class="columns">
<div class="column is-one-third">

Solstice porte **vos formations**.

[Contactez-nous](/contact/#organisme-de-formation){ .button .is-cta }

</div>

<div class="column">

Nous avons mis en place la possibilité de porter les formations de coopératives ou de structures locales, avec lesquelles nous partageons les mêmes valeurs et qui ne peuvent pas ou ne souhaitent pas devenir organisme de formation certifié Qualiopi.

Nous nous réjouissons de porter d'ores et déjà les formations de [Villages Vivants](https://villagesvivants.com/), [Dromolib](https://dromolib.fr/) et Arcen Ciel.
</div>
</div>

## Vous avez besoin d’être formé·e au métier de formateur·trice ?

<div class="columns">
<div class="column is-one-third">

Solstice vous propose une **formation en trois modules**.

[Vous souhaitez recevoir le programme ? Contactez-nous](/contact/#formation-formateurice){ .button .is-cta }
</div>

<div class="column">

Que votre activité de formation soit hébergée ou non chez Solstice, cette formation est pour vous si vous êtes :
- formateur·trice occasionnel·le
- formatrice·teur confirmé·e
- nouvelle et nouveau formatrice·teur

**Pourquoi ?**
- Acquérir ou réviser les outils de base
- Revoir sa pratique sous un autre angle
- Mettre à jour ses connaissances sur la formation pro
- Animer efficacement une formation
- Structurer professionnellement son offre de formation (ingénierie)

</div>
</div>

:::info Qu'est-ce que Qualiopi ?
Qualiopi permet aux bénéficiaires de la formation de continuer à solliciter des financements publics pour leurs prises en charge de formation et de garantir un niveau de qualité élevé des prestations.

La démarche de certification Qualiopi de Solstice est portée par les entrepreneur·e·s formateur·trices et l’équipe support. L’objectif est de structurer l’exercice de ce métier pour répondre aux évolutions du marché de la Formation Professionnelle Continue.
:::
