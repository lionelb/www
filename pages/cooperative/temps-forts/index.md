---
title: Des temps forts qui ont marqué la vie coopérative
eleventyNavigation:
  key: Les temps forts
  parent: Vie coopérative
---

# Vie coopérative

S’informer, se rencontrer, découvrir la coopérative.

[Les prochains événements](/cooperative/agenda/){ .button .is-cta }

[Les temps forts](/cooperative/temps-forts/){ .button .is-cta .is-active }

{% for post in collections.posts %}
<div class="card card--post mb-4 temps-fort">
  <header class="card-header">
    <p class="card-header-title">
      <a href="{{ post.url }}" rel="permalink">
        <time datetime="{{ post.date.toISOString() }}">{{ post.date | toDate }}</time>
        <span>{{ post.data.title }}</span>
      </a>
    </p>
  </header>
</div>
{% endfor %}
