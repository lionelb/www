module.exports = {
  eleventyComputed: {
    eleventyNavigation: {
      key: data => data.eleventyNavigation.key || data.title,
      parent: data => data.eleventyNavigation.parent || 'Les temps forts'
    }
  }
}
