---
title: Événements, ateliers et entraide coopérative
eleventyNavigation:
  key: Les prochains événements
  parent: Vie coopérative
---

# Vie coopérative

S’informer, se rencontrer, découvrir la coopérative et faire communauté.

[Les prochains événements](/cooperative/agenda/){ .button .is-cta .is-active }

[Les temps forts// restitutions](/cooperative/temps-forts/){ .button .is-cta }

## Réunions d'information collective

<div class="tile is-ancestor agenda">
{% for event in calendars.infoColl %}
<div class="tile is-4">
<div class="card card--event">
  <div class="card-content">
    <div class="content">
      <span class="icon"><i class="fas fa-calendar"></i></span>
      <b>{{ event.startDate | toDateTime }}</b> ({{ event.duration | durationToTime }})

      {{ event.location | location('en visio', 'plan et directions') | safe }}
    </div>
  </div>
  <footer class="card-footer">
    <a href="{{ '/nous-rejoindre/reunion-information/' }}?event={{ event.uid }}" class="card-footer-item">Je m'inscris</a>
    <!-- <a href="#" class="card-footer-item">Ajouter à mon agenda</a> -->
  </footer>
</div>
</div>
{% endfor %}
</div>

## Ateliers, formations et rencontres coopératives

<div class="tile is-ancestor agenda">
{% for event in calendars.vieCooperative %}
<div class="tile is-4">
<div class="card card--event">
  <div class="card-content">
    <div class="content">
      <p class="is-size-5 has-text-weight-bold">{{ event.summary }}</p>
      {%- if event.description -%}
        <p class="is-size-6">{{ event.description }}</p>
      {%- endif -%}
      <p>
      <span class="icon"><i class="fas fa-calendar"></i></span>
      {%- if event.duration.toSeconds() === 86400 -%}
      <b>{{ event.startDate | toDate }} {{ event.duration | durationToTime }}</b>
      {%- else -%}
      <b>{{ event.startDate | toDateTime }}</b>
        {%- if event.duration.hours > 0 %}
          ({{ event.duration | durationToTime }})
        {% endif -%}
      {%- endif -%}
      {%- if event.location -%}
      <br>{{ event.location | location('en visio', 'plan et directions') | safe }}
      {%- endif -%}
      </p>
    </div>
  </div>
</div>
</div>
{% endfor %}
</div>
